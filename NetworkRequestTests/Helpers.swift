//
//  Helpers.swift
//  NetworkRequestTests
//
//  Created by Sergio Andres Rodriguez Castillo on 17/01/24.
//

import Foundation
import UIKit

func tap(_ button: UIButton) {
    button.sendActions(for: .touchUpInside)
}
