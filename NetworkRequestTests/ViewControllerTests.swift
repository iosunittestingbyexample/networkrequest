//
//  ViewControllerTests.swift
//  NetworkRequestTests
//
//  Created by Sergio Andres Rodriguez Castillo on 17/01/24.
//

import XCTest
@testable import NetworkRequest

final class ViewControllerTests: XCTestCase {
    var sut: ViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(identifier: String(describing: ViewController.self))
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_tappintButton_shouldMakeDataTaskToSearchForEBookOutFromBoneville() {
        let mockURLSession = MockURLSession()
        sut.session = mockURLSession
        sut.loadViewIfNeeded()
        
        tap(sut.button)
        
        mockURLSession.verifyDataTask(with: URLRequest(url: URL(string: "https://itunes.apple.com/search?media=ebook&term=out%20from%20boneville")!))
    }
}
